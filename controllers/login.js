var express = require('express');
var app = express();
var router = express.Router();
var models = require('../models/schema');
var session = require('express-session');
var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy;
var Product = require('../models/product');

passport.use(new LocalStrategy(
  function(username, password, done) {
    models.User.findOne({ username: username }, function (err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false);
      }
      if (user.password != password) {
        return done(null, false);
      }
      return done(null, user);
    });
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  models.User.findById(id, function(err, user) {
    done(err, user);
  });
});

router.route('/home').get(function(req,res,next){
      Product.find(function(err, docs){

        var productChunks = [];
        var chunkSize = 3;

        for (var i = 0; i < docs.length; i += chunkSize){
            productChunks.push(docs.slice(i, i + chunkSize));
        }

          res.render('home',{ title: 'Shopping Cart', products: productChunks });
    });
});


router.route('/login')
.get(function(req,res){
	res.render('login');
})
.post(passport.authenticate('local', { successRedirect: '/user/home',
                                   failureRedirect: '/user/login',
                                   failureFlash: true }))

module.exports = router;
